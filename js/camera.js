'use strcit';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;
var components = app.components;

function getCamera(resolve, reject) {
    var options = {
        name: 'camera',
        template: 'templates/camera.html',
        data: function () {
            return {
                selected: null,
                viewData: null,
                panorama: false
            };
        },
        components: {
            'camera-view': components.getView
        },
        methods: {
            popup: app.popup
        }
    };
    asyncComponent(options, resolve, reject);
}

app.components.getCamera = getCamera;

})(app);
