'use strcit';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getMap(resolve, reject) {
    var options = {
        name: 'map',
        template: 'templates/map.html',
        data: function () {
            return {
                style: {} 
            };
        },
        mounted: function () {
            initMap();
        }
    };
    asyncComponent(options, resolve, reject);
}

function initMap() {
    var map = new BMap.Map('map-bd');
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
    map.addControl(new BMap.MapTypeControl());
    map.setCurrentCity('北京');
    map.enableScrollWheelZoom(true);
}

app.components.getMap = getMap;

})(app);

