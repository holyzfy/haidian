'use strict';

var app = {
    components: {},
    _debug: {}
};

(function (app) {

function getEnv() {
    var env = {
        '127.0.0.1': 'development',
        'localhost': 'development',
        'static.f2e.yiifcms.com': 'development'
    };
    return env[location.hostname] || 'production';
}

var urlMap = {};

urlMap.development = {

};

// 生产环境的接口地址
urlMap.production = {

};

function mountDialog(options, callback) {
    $.get(options.template, function (html) {
        $.extend(options, {template: html});
        var Dialog = Vue.extend(options);
        var instance = new Dialog({
            el: document.createElement('div')
        });
        $('body').append(instance.$el);
        $.isFunction(callback) && callback(instance);
    });
}

function asyncComponent(options, resolve, reject) {
    var url = options.template;
    asyncComponent.cache = asyncComponent.cache || {};
    
    var promise;
    if(asyncComponent.cache[url]) {
        promise = $.Deferred().resolve(asyncComponent.cache[url]).promise();
    } else {
        promise = $.get(url);
    }

    promise.then(function (html) {
        asyncComponent.cache[url] = html;
        var component = $.extend({}, options, {template: html});
        resolve(component);
    }).fail(function (xhr, status, error) {
        reject(error);
    });
}

if(getEnv() === 'production') {
    Vue.config.silent = true;
    Vue.config.productionTip = false;
    Vue.config.errorHandler = $.noop;
}

app.common = {
    urlMap: urlMap[getEnv()],
    mountDialog: mountDialog,
    asyncComponent: asyncComponent
};

})(app);

