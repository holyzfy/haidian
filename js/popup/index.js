'use strict';

(function (app) {

var common = app.common;

function popup() {
    var options = {
        name: 'popup',
        template: 'templates/popup/index.html',
        data: function () {
            return {
                selected: true
            };
        },
        methods: {
            share: app.popupShare,
            feed: app.popupFeed,
            mark: app.popupMark
        }
    };
    common.mountDialog(options, function (instance) {
        app._debug.popup = instance;
    });
}

app.popup = popup;

})(app);
