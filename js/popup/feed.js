'use strict';

(function (app) {

var common = app.common;

function popupFeed() {
    var options = {
        name: 'feed',
        template: 'templates/popup/feed.html',
        data: function () {
            return {
                selected: true
            };
        },
        methods: {
            close: function () {
                this.selected = false;
            },
            submit: function () {
                this.close();
            }
        }
    };
    common.mountDialog(options, function (instance) {
        app._debug.feed = instance;
    });
}

app.popupFeed = popupFeed;

})(app);
