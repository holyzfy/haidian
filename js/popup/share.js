'use strict';

(function (app) {

var common = app.common;

function popupShare() {
    var options = {
        name: 'share',
        template: 'templates/popup/share.html',
        data: function () {
            return {
                selected : true,
                tab: '委办单位'
            };
        },
        methods: {
            close: function () {
                this.selected = false;
            },
            submit: function () {
                this.close();
            }
        }
    };
    common.mountDialog(options, function (instance) {
        app._debug.popupShare = instance;
    });
}

app.popupShare = popupShare;

})(app);
