'use strcit';

(function (app) {

var components = app.components;

function start() {
    app.instance = new Vue({
        el: '#app',
        components: {
            maps: components.getMap,
            search: components.getSearch,
            camera: components.getCamera
        }
    });
}

app.start = start;

})(app);
