'use strcit';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getSearch(resolve, reject) {
    var options = {
        name: 'search',
        template: 'templates/search.html',
        data: function () {
            return {
                type: '', // 'text' or 'voice'
                keyword: '',
                suggest: []
            };
        },
        watch: {
            keyword: function (keyword) {
                this.suggest = keyword.split('');
            }
        },
        methods: {
            hideSearch: function () {
                this.type = '';
            },
            showItem: function (item, index) {
                alert('你点击了地图的第' + index + '个搜索结果');
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

app.components.getSearch = getSearch;

})(app);
