'use strcit';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getView(resolve, reject) {
    var options = {
        name: 'view',
        template: 'templates/view.html',
        props: ['view-data'],
        methods: {
            popup: app.popup
        }
    };
    asyncComponent(options, resolve, reject);
}

app.components.getView = getView;

})(app);
